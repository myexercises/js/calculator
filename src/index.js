import "./styles.scss";
import handleClick from './modules/handleKeyboard';

let globalState = {
  expression: [],
  result: 0,
  
  updateResult(value) { 
    this.result = value
  },
  
  updateExpression(value) { 
    this.expression = value
  },
  
  clear() {
    this.updateExpression([]);
    this.updateResult(0);
  },

  save() {
    console.log('State saved!');
  }
};

const keyboard = document.getElementById('keyboard');
  
function handleKeyboardClick() {
  keyboard.addEventListener('click', event => {
    handleClick(event, globalState);
  });
}

function main() {
  handleKeyboardClick();
}

main();