import { displayExpression, displayResult } from './display';

export const clearState = (state) => {
    state.clear();
    displayExpression(state.expression);
    displayResult(state.result);
}

export const saveState = (state) => state.save();
