import { operationsList, symbolFactory } from './constants';

const operationsRow = document.getElementById('operations');  
const resultRow = document.getElementById('result');

const mapOperationsToSymbols = expression => 
    expression.length === 0
        ? ['0']
        : expression.map(element =>
            operationsList.includes(element) ? symbolFactory(element) : element
        );


export function displayResult(result) {
    resultRow.innerHTML = result;
}

export function displayExpression(expression) {
    const expressionWithSymbols = mapOperationsToSymbols(expression);
    operationsRow.innerHTML = expressionWithSymbols.join(' ');
}