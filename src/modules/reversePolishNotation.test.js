import { convertIntoRPN, calculateRPNExpression, notArray } from './reversePolishNotation';

const notArrayArgument = () => convertIntoRPN('foo');

describe('Reverse Polish Notation test suite', () => {
    describe('converts expression into RPN', () => {
        it('throws an error expression is not an array', () => {
            expect(notArrayArgument).toThrowError(notArray);
        })

        it('converts [1, add, 2] into [1, 2, add]', () => {
            expect(convertIntoRPN(['1', 'add', '2'])).toEqual(['1', '2', 'add']);
        });
        it('converts [2,add,3,multiple,4, subtract,4, divide,2] into [2,3,4,multiple,add,4,2,divide,subtract]', () => {
            expect(convertIntoRPN(['2', 'add', '3', 'multiple', '4', 'subtract', '4', 'divide', '2']))
                .toEqual(['2', '3', '4', 'multiple', 'add', '4', '2', 'divide', 'subtract']);
        });
    });

    describe('calculates RPN expression', () => {
        it('returns 3 for [1, 2, add]', () => {
            expect(calculateRPNExpression(['1', '2', 'add'])).toBe(3);
        });
        it('returns 14 for [2, 3, 4, multiple, add]', () => {
            expect(calculateRPNExpression(['2', '3', '4', 'multiple', 'add'])).toBe(14);
        });
        it('returns 12 for [2,3,4,multiple,add,4,2,divide,subtract]', () => {
            expect(calculateRPNExpression(['2', '3', '4', 'multiple', 'add', '4', '2', 'divide', 'subtract'])).toBe(12);
        });
        
    });
    describe('calculates RPN expression from normal expression', () => {
        it('returns 3 for [1, add, 2]', () => {
            const rpn = convertIntoRPN(['1', 'add', '2']);
            expect(calculateRPNExpression(rpn)).toBe(3);
        });
        it('returns 12 for [2,add,3,multiple,4, subtract,4, divide,2]', () => {
            const rpn = convertIntoRPN(['2', 'add', '3', 'multiple', '4', 'subtract', '4', 'divide', '2']);
            expect(calculateRPNExpression(rpn)).toBe(12);
        });
    });
});