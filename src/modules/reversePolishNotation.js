import { add, subtract, multiple, divide } from './arithmeticOperations';
import { arithmeticOp, calculationFactory } from './constants';

export const notArray = 'argument should be of array type';
const PRIORITY = {
    add: 1,
    subtract: 1,
    multiple: 2,
    divide: 2,
}


export function convertIntoRPN(expression = []) {
    let stack = [];
    let result = [];

    function processOperation(element) {
        while ( stack.length > 0 && PRIORITY[element] <= PRIORITY[stack[stack.length - 1]]) {
            let last = stack.pop();
            result.push(last);
        }
        stack.push(element);
    }

    function processElement(element) {
        if (!isNaN(element)) {
            result.push(element);
        }
        if (arithmeticOp.includes(element)) {
            processOperation(element);
        }
    }

    if (typeof expression !== 'object' && !Array.isArray(expression)) {
        throw new Error(notArray);
    };

    expression.forEach(element => processElement(element));
    result = [...result, ...stack.reverse()];
    return result;

}

export function calculateRPNExpression(rnpExpression) {
    let stack = [];
    let result = 0;

    function doCalculation(operation) {
        let b = parseFloat(stack.pop());
        let a = parseFloat(stack.pop());
        const calculate = calculationFactory(operation);
        return calculate(a, b);
    }
    function processRnp(element) {
        if (!isNaN(element)) {
            stack.push(element);
        }
        if (arithmeticOp.includes(element)) {
            stack.push(doCalculation(element));
        }
    }

    rnpExpression.forEach(element => processRnp(element));
    return stack.pop();
}