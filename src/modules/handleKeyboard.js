import { displayExpression, displayResult } from './display';
import { calculationFactory, KEY, SPECIALS } from './constants';
import {convertIntoRPN, calculateRPNExpression } from "./reversePolishNotation";



let expression = [];
let element = '';

const value = event => event.target.attributes['data-key'].value;

function solveExpression(expression) {
    const expressionWithoutLast = expression.slice(0, expression.length - 2);
    const infixToPostfix = convertIntoRPN(expression);
    return calculateRPNExpression(infixToPostfix);
}

function setNumberValue(current, added) {
    if (added !== '.') {
        return current + added;
    }
    return (current.includes('.')) ? current : current + added;
}

export default function handleClick(event, state) {
    if (event.target.matches(KEY.NUMBER)) {
        if (state.result) {
            element = value(event);
            expression = [...expression, element];
            state.updateResult(0);
            displayResult(state.result);
        } else if (element !== '') {
            const expressionCopy = [...expression];
            expressionCopy.pop();
            element = setNumberValue(element, value(event));
            expression = [...expressionCopy, element];
        } else {
            element = element + value(event);
            expression = [...expression, element];
        }
        state.updateExpression(expression);
    }
    if (event.target.matches(KEY.OPERATION)) {
        if (state.result) {
            expression = [state.result, value(event)];
            state.updateResult(0);
            displayResult(state.result);
        } else {
            expression = [...expression, value(event)];
            element = '';
        };
        state.updateExpression(expression);
    }
    if (event.target.matches(KEY.SPECIAL)) {
        SPECIALS[value(event)](state);
    }
    if (event.target.matches(KEY.RESULT)) {
        expression = [...expression, value(event)];
        element = '';
        const result = solveExpression(expression);
        state.updateExpression(expression);
        state.updateResult(result);
        expression = [];
        displayResult(state.result);
    }
    displayExpression(state.expression);
}