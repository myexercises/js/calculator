import { add, subtract, multiple, divide } from './arithmeticOperations';
import { clearState, saveState } from './stateOperations';

export const arithmeticOp = ['subtract', 'add', 'multiple', 'divide'];

export const KEY = {
    NUMBER: '.key__number',
    OPERATION: '.key__operation',
    SPECIAL: '.key__special',
    RESULT: '.key__result',
};

export const SPECIALS = {
    clear: clearState,
    save: saveState,
}

export const OPERATIONS = {
    add: {
        symbol: '+',
        operation: add,
    },
    subtract: {
        symbol: '-',
        operation: subtract,
    },
    multiple: {
        symbol: 'x',
        operation: multiple,
    },
    divide: {
        symbol: '\u00F7',
        operation: divide,
    },
    result: {
        symbol: '=',
    },
}

export const operationsList = Object.keys(OPERATIONS);
export const calculationFactory = operation => OPERATIONS[operation].operation;
export const symbolFactory = operation => OPERATIONS[operation].symbol;
