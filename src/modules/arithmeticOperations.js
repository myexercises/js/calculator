export const numberValuesError = 'both values should be of number type';
export const divisionByZero = 'division by zero';

const isNumber = num => typeof num === 'number';

export const add = (a, b) => {
    if (isNumber(a) && isNumber(b)) {
        return a + b;
    }
    throw new Error(numberValuesError);
};

export const subtract = (a, b) => {
    if (isNumber(a) && isNumber(b)) {
        return a - b;
    }
    throw new Error(numberValuesError);
};

export const multiple = (a, b) => {
    if (isNumber(a) && isNumber(b)) {
        return (a === 0 || b === 0) ? 0 : a * b;
    }
    throw new Error(numberValuesError);
};

export const divide = (a, b) => {
    if (isNumber(a) && isNumber(b)) {
        if (Math.abs(b) === 0) throw new Error(divisionByZero)
        return (a === 0) ? 0 : a / b;
    }
    throw new Error(numberValuesError);
};