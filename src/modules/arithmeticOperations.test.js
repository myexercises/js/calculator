import { add, subtract, multiple, divide, divisionByZero, numberValuesError } from './arithmeticOperations';

const bothNotNumbers = operation => () => operation('a', undefined);
const firstNotNumber = operation => () =>  operation(null, 2);
const secondNotNumber = operation => () =>  operation(2, {});
const divideByZero =() =>  divide(5, 0);

describe('basic operations test suite', () => {
  
  describe('add', () => {
    it('add(2,3) should return 5', () => {
      expect(add(2, 3)).toBe(5);
    })
    it('add 5 and 0 returns 5', () => {
      expect(add(5, 0)).toBe(5);
      expect(add(0, 5)).toBe(5);
    })
    it('throws an error when both values are not numbers', () => {
      expect(bothNotNumbers(add)).toThrowError(numberValuesError);
    })
    it('throws an error when first value are not numbers', () => {
      expect(firstNotNumber(add)).toThrowError(numberValuesError);
    })
    it('throws an error when second value are not numbers', () => {
      expect(secondNotNumber(add)).toThrowError(numberValuesError);
    })
  });

  describe('subtract', () => {
    it('subtract(3,1) should return 1', () => {
      expect(subtract(3, 1)).toBe(2);
    })
    it('5 subtract 0 returns 5', () => {
      expect(subtract(5, 0)).toBe(5);
    })
    it('0 subtract 5 returns -5', () => {
      expect(subtract(0, 5)).toBe(-5);
    })
    it('0 subtract -5 returns 5', () => {
      expect(subtract(0, 5)).toBe(-5);
    })
    it('throws an error when both values are not numbers', () => {
      expect(bothNotNumbers(subtract)).toThrowError(numberValuesError);
    })
    it('throws an error when first value are not numbers', () => {
      expect(firstNotNumber(subtract)).toThrowError(numberValuesError);
    })
    it('throws an error when second value are not numbers', () => {
      expect(secondNotNumber(subtract)).toThrowError(numberValuesError);
    })
  });

  describe('multiple', () => {
    it('multiple 3 and 1 should return 3', () => {
      expect(multiple(3, 1)).toBe(3);
      expect(multiple(1, 3)).toBe(3);
    })
    it('multiple 0 and 5 returns 0', () => {
      expect(multiple(5, 0)).toBe(0);
      expect(multiple(0, 5)).toBe(0);
    })
    it('multiple 0 and -5 returns 0', () => {
      expect(multiple(0, -5)).toBe(0);
      expect(multiple(-5, 0)).toBe(0);
    })
    it('throws an error when both values are not numbers', () => {
      expect(bothNotNumbers(multiple)).toThrowError(numberValuesError);
    })
    it('throws an error when first value are not numbers', () => {
      expect(firstNotNumber(multiple)).toThrowError(numberValuesError);
    })
    it('throws an error when second value are not numbers', () => {
      expect(secondNotNumber(multiple)).toThrowError(numberValuesError);
    })
  });

  describe('divide', () => {
    it('divide(3,1) should return 3', () => {
      expect(divide(3, 1)).toBe(3);
    })
    it('divide(1,3) should return 0.333333', () => {
      expect(divide(1, 3)).toBe(0.3333333333333333);
    })
    it('divide(0,5) returns 0', () => {
      expect(divide(0, 5)).toBe(0);
    })
    it('divide(0, -5) returns 0', () => {
      expect(divide(0, -5)).toBe(0);
    })
    it('divide by 0 throws an error', () => {
      expect(divideByZero).toThrowError(divisionByZero);
    })
    it('throws an error when both values are not numbers', () => {
      expect(bothNotNumbers(divide)).toThrowError(numberValuesError);
    })
    it('throws an error when first value are not numbers', () => {
      expect(firstNotNumber(divide)).toThrowError(numberValuesError);
    })
    it('throws an error when second value are not numbers', () => {
      expect(secondNotNumber(divide)).toThrowError(numberValuesError);
    })
  });

});